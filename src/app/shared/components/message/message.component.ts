import { Component, Input } from '@angular/core';
import { LoginFacade } from 'src/app/pages/login/store/facade';
import { IUser } from '../../interfaces/user';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent {

  @Input() data: string;
  @Input() date: string;
  @Input() sender: string;
  allUsers$ = this.loginFacade.allUsers$;

  constructor(private loginFacade: LoginFacade){}

  getUserName(allUsers: IUser[], sender: string): string{
    return allUsers.find(user => `${user.id}` === sender)?.name;
  }
}
