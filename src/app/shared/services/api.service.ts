import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IRoom } from '../interfaces/room';
import { IMessage } from '../interfaces/message';
import { IUser } from '../interfaces/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private _http: HttpClient) {}

  getRooms(): Observable<IRoom[]> {
    return this._http.get<IRoom[]>(`${environment.apiUrl}/api/rooms`);
  }
  getRoom(roomId: string): Observable<IRoom> {
    return this._http.get<IRoom>(`${environment.apiUrl}/api/rooms/${roomId}`);
  }
  addRoom(room: { name: string; password: string }): Observable<IRoom> {
    return this._http.post<IRoom>(`${environment.apiUrl}/api/rooms`, room);
  }
  getMessages(roomId: string): Observable<IMessage[]> {
    return this._http.get<IMessage[]>(
      `${environment.apiUrl}/api/rooms/${roomId}/messages`
    );
  }
  addMessage(item: {
    data: string;
    sender: number;
    roomId: number;
  }): Observable<IMessage> {
    return this._http.post<IMessage>(
      `${environment.apiUrl}/api/rooms/${item.roomId}/messages`,
      { data: item.data, sender: item.sender }
    );
  }
  login(user: {
    email: string;
    password: string;
  }): Observable<Record<string, unknown>> {
    return this._http.post<Record<string, unknown>>(
      `${environment.apiUrl}/api/login`,
      { email: user.email, password: user.password }
    );
  }
  logout(): Observable<Record<string, unknown>> {
    return this._http.delete<Record<string, unknown>>(
      `${environment.apiUrl}/api/logout`
    );
  }
  signup(user: IUser): Observable<IUser> {
    return this._http.post<IUser>(
      `${environment.apiUrl}/api/signup`,
      { name: user.name, email: user.email, password: user.password }
    );
  }
  getUser(): Observable<IUser> {
    return this._http.get<IUser>(`${environment.apiUrl}/api/me`);
  }
  listUsers(): Observable<IUser[]> {
    return this._http.get<IUser[]>(`${environment.apiUrl}/api/users`);
  }
}
