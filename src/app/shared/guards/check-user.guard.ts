import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { map, skipWhile } from 'rxjs/operators';
import { LoginFacade } from 'src/app/pages/login/store/facade';

@Injectable({ providedIn: 'root' })
export class CheckUserGuard implements CanActivate {
  constructor(private loginFacade: LoginFacade, private router: Router) {}

  canActivate() {
    this.loginFacade.getUser();
    this.loginFacade.listUsers();
    return this.loginFacade.user$.pipe(
      skipWhile(user => user === undefined),
      map(user => {
        if (!user) {
          void this.router.navigate(['/']);
          return false;
        }
        return true;
      }),
    );
  }
}
