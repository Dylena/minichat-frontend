import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { LoginEffects } from '../pages/login/store/effects';
import { LoginFacade } from '../pages/login/store/facade';
import { loginReducer } from '../pages/login/store/reducer';
import { MessageComponent } from './components/message/message.component';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forFeature('login', loginReducer),
    EffectsModule.forFeature([LoginEffects])
  ],
  exports: [
    HttpClientModule,
    ReactiveFormsModule,
    MessageComponent
  ],
  declarations: [
    MessageComponent,
  ],
  providers: [LoginEffects, LoginFacade]
})
export class SharedModule {}
