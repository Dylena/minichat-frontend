export interface IMessage {
  id?: number;
  data: string;
  date?: string | Date;
  sender: number; //userId
  roomId?: number;
}
