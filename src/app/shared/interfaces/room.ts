export interface IRoom {
  id: number;
  name: string;
  password: string;
}
