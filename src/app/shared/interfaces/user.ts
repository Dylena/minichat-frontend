export interface IUser {
  name?: string;
  email?: string;
  id?: number;
  password?: string;
}
