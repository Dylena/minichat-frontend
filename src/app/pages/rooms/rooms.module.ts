import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomsRoutingModule } from './rooms-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { roomsReducer } from './store/reducer';
import { RoomsEffects } from './store/effects';
import { RoomsFacade } from './store/facade';
import { RoomsGuard } from './rooms.guard';
import { RoomsComponent } from './components/rooms.component';
import { RoomCreationModalComponent } from './components/room-creation-modal/room-creation-modal.component';


@NgModule({
  declarations: [RoomsComponent, RoomCreationModalComponent],
  imports: [
    CommonModule,
    RoomsRoutingModule,
    SharedModule,
    StoreModule.forFeature('rooms', roomsReducer),
    EffectsModule.forFeature([RoomsEffects])
  ],
  providers: [RoomsEffects, RoomsFacade, RoomsGuard]
})
export class RoomsModule { }
