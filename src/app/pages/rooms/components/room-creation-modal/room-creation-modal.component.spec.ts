import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomCreationModalComponent } from './room-creation-modal.component';

describe('RoomCreationModalComponent', () => {
  let component: RoomCreationModalComponent;
  let fixture: ComponentFixture<RoomCreationModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoomCreationModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomCreationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
