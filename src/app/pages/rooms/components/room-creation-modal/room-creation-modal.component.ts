import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-signup-modal',
  templateUrl: './room-creation-modal.component.html',
  styleUrls: ['./room-creation-modal.component.scss']
})
export class RoomCreationModalComponent {

  @Input() title = 'Title';
  @Input() image = '';
  @Input() body = '';


  roomCreationForm = this._fb.group({
    name: ['', Validators.required],
  });

  constructor(public activeModal: NgbActiveModal, private _fb: FormBuilder,) { }

}
