import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginFacade } from '../../login/store/facade';
import { RoomsFacade } from '../store/facade';
import { RoomCreationModalComponent } from './room-creation-modal/room-creation-modal.component';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss'],
})
export class RoomsComponent {
  rooms$ = this.roomsFacade.rooms$;
  constructor(
    private router: Router,
    private roomsFacade: RoomsFacade,
    private ngbModal: NgbModal,
    private loginFacade: LoginFacade
  ) {}

  add(): void {
    const modal = this.ngbModal.open(RoomCreationModalComponent, {
      centered: true,
    });
    modal.componentInstance.title = 'Create New Room';
    modal.result
      .then(({ name, password }) => {
        this.roomsFacade.addRoom({ name, password });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  details(id: string): void {
    this.router.navigate(['rooms/details', id]);
  }

  logout(): void {
    this.loginFacade.logout();
    this.loginFacade.isLoggedIn$.subscribe((status) => {
      if (!status) {
        this.router.navigate(['/login']);
      }
    });
  }
}
