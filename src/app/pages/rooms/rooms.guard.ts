import { Injectable } from '@angular/core';
import { CanActivate, CanDeactivate } from '@angular/router';
import { RoomsFacade } from './store/facade';

@Injectable()
export class RoomsGuard implements CanActivate, CanDeactivate<void> {
  constructor(private roomsFacade: RoomsFacade) {}
  canActivate(): boolean {
    this.roomsFacade.getRooms();
    return true;
  }

  canDeactivate(): boolean {
    this.roomsFacade.resetRooms();
    return true;
  }
}
