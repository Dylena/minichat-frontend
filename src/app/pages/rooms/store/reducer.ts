import { Action, createReducer, on } from '@ngrx/store';
import { addRoom, getRooms, resetRooms } from './actions';
import { initialRoomsState, IRoomsState } from './state';

const reducer = createReducer<IRoomsState>(
  initialRoomsState,
  on(getRooms.success, (state, { rooms }) => ({
    ...state,
    rooms
  })),
  on(addRoom.success, (state, { room }) => ({
    ...state,
    rooms: [ ...state.rooms, room ]
  })),
  on(resetRooms, state => ({
    ...state,
    rooms: []
  })),
);

export function roomsReducer(state: IRoomsState, action: Action) {
  return reducer(state, action);
}
