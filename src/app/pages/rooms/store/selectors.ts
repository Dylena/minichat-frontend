import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IRoomsState } from './state';

const featureSelector = createFeatureSelector<IRoomsState>('rooms');

const getRooms = createSelector(featureSelector, state => state.rooms);

export const roomsQuery = {
  getRooms
};
