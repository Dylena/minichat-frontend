import { IRoom } from "src/app/shared/interfaces/room";

export const initialRoomsState: IRoomsState = {
  rooms: [],
};

export interface IRoomsState {
  rooms: IRoom[];
}
