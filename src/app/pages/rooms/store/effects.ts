import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/services/api.service';
import { addRoom, getRooms } from './actions';

@Injectable()
export class RoomsEffects {
  getRooms$ = createEffect(() =>
    this._actions$.pipe(
      ofType(getRooms.fetch),
      switchMap(() => {
        return this._apiService.getRooms().pipe(
          map(rooms => {
            return getRooms.success({ rooms });
          }),
          catchError(err => {
            return of(getRooms.fail({ error: err.message }));
          })
        );
      })
    )
  );

  addRoom$ = createEffect(() =>
    this._actions$.pipe(
      ofType(addRoom.fetch),
      switchMap(({ name, password }) => {
        return this._apiService.addRoom({ name, password }).pipe(
          map(response => {
            return addRoom.success({ room: response });
          }),
          catchError(err => of(addRoom.fail({ error: err.message })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _apiService: ApiService) {}
}
