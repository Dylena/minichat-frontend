import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { addRoom, getRooms, resetRooms } from './actions';
import { roomsQuery } from './selectors';

@Injectable()
export class RoomsFacade {
  rooms$ = this._store.pipe(select(roomsQuery.getRooms));

  constructor(private _store: Store) {}

  getRooms(): void {
    this._store.dispatch(getRooms.fetch());
  }

  addRoom(room: { name: string; password: string }): void {
    this._store.dispatch(addRoom.fetch(room));
  }

  resetRooms(): void {
    this._store.dispatch(resetRooms());
  }
}
