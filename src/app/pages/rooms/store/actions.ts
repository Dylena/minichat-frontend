import { createAction, props } from '@ngrx/store';
import { IRoom } from 'src/app/shared/interfaces/room';
import { createApiAction } from 'src/app/shared/utils/ngrx.utils';

/*export const getSchemaItem = createApiAction('[Schema Actions] getSchemaItem', {
  fetch: props<{ id: string }>(),
  success: props<{ schema: IApiResponse<ISchemaItem> }>(),
  fail: props<{ error: string }>()
});

export const resetSchemaItem = createAction('[Schema Actions] resetSchemaItem');*/

export const getRooms = createApiAction('[Rooms Actions] getRooms', {
  success: props<{ rooms: IRoom[] }>(),
  fail: props<{ error: string }>()
});

export const addRoom = createApiAction('[Rooms Actions] addRooms', {
  fetch : props<{ name: string, password: string }>(),
  success: props<{ room: IRoom }>(),
  fail: props<{ error: string }>()
});

export const resetRooms = createAction('[Rooms Actions] resetRooms');
