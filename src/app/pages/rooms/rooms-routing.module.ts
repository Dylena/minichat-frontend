import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckUserGuard } from 'src/app/shared/guards/check-user.guard';
import { RoomsComponent } from './components/rooms.component';
import { RoomsGuard } from './rooms.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [RoomsGuard, CheckUserGuard],
    canDeactivate: [RoomsGuard],
    component: RoomsComponent
  },
  {
    path: 'details/:id',
    loadChildren: () => import('src/app/pages/room-details/room-details.module').then(m => m.RoomDetailsModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomsRoutingModule { }
