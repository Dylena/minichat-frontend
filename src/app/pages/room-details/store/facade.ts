import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { IMessage } from 'src/app/shared/interfaces/message';
import { addMessage, getMessages, getRoom, receiveMessage, resetRoom } from './actions';
import { roomDetailsQuery } from './selectors';

@Injectable()
export class RoomDetailsFacade {
  room$ = this._store.pipe(select(roomDetailsQuery.getRoom));
  messages$ = this._store.pipe(select(roomDetailsQuery.getMessages));

  constructor(private _store: Store) {}

  getRoom(roomId: string): void {
    this._store.dispatch(getRoom.fetch({ roomId }));
  }

  getMessages(roomId: string): void {
    return this._store.dispatch(getMessages.fetch({ roomId }));
  }

  addMessage(item: { data: string; sender: number; roomId: number }): void {
    this._store.dispatch(addMessage.fetch(item));
  }

  receiveMessage(message: IMessage): void {
    this._store.dispatch(receiveMessage({message}));
  }

  resetRoom(): void {
    this._store.dispatch(resetRoom());
  }
}
