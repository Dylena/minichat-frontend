import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ApiService } from 'src/app/shared/services/api.service';
import { addMessage, getMessages, getRoom } from './actions';

@Injectable()
export class RoomDetailsEffects {
  getRooms$ = createEffect(() =>
    this._actions$.pipe(
      ofType(getRoom.fetch),
      switchMap(({roomId}) => {
        return this._apiService.getRoom(roomId).pipe(
          map(room => {
            return getRoom.success({ room });
          }),
          catchError(err => {
            return of(getRoom.fail({ error: err.message }));
          })
        );
      })
    )
  );

  getMessages$ = createEffect(() =>
    this._actions$.pipe(
      ofType(getMessages.fetch),
      switchMap(({roomId}) => {
        return this._apiService.getMessages(roomId).pipe(
          map(messages => {
            const m = [...messages.map(item => ({...item, date: new Date(item.date)}))]
            return getMessages.success({ messages: m });
          }),
          catchError(err => {
            return of(getMessages.fail({ error: err.message }));
          })
        );
      })
    )
  );

  addMessage$ = createEffect(() =>
    this._actions$.pipe(
      ofType(addMessage.fetch),
      switchMap((item) => {
        return this._apiService.addMessage(item).pipe(
          map(message => {
            return addMessage.success({ message });
          }),
          catchError(err => {
            return of(addMessage.fail({ error: err.message }));
          })
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _apiService: ApiService) {}
}
