import { createAction, props } from '@ngrx/store';
import { IMessage } from 'src/app/shared/interfaces/message';
import { IRoom } from 'src/app/shared/interfaces/room';
import { createApiAction } from 'src/app/shared/utils/ngrx.utils';

/*export const getSchemaItem = createApiAction('[Schema Actions] getSchemaItem', {
  fetch: props<{ id: string }>(),
  success: props<{ schema: IApiResponse<ISchemaItem> }>(),
  fail: props<{ error: string }>()
});

export const resetSchemaItem = createAction('[Schema Actions] resetSchemaItem');*/

export const getRoom = createApiAction('[Room Details Actions] getRoom', {
  fetch: props<{ roomId: string }>(),
  success: props<{ room: IRoom }>(),
  fail: props<{ error: string }>()
});

export const getMessages = createApiAction('[Room Details Actions] getMessages', {
  fetch: props<{ roomId: string }>(),
  success: props<{ messages: IMessage[] }>(),
  fail: props<{ error: string }>()
});

export const addMessage = createApiAction('[Room Details Actions] addMessage', {
  fetch: props<{ data: string, sender: number, roomId: number }>(),
  success: props<{ message: IMessage }>(),
  fail: props<{ error: string }>()
});

export const receiveMessage = createAction('[Rooms Details Actions] receiveMessage', props<{ message: IMessage }>(),);

export const resetRoom = createAction('[Rooms Details Actions] resetRoom');
