import { Action, createReducer, on } from '@ngrx/store';
import { getMessages, getRoom, receiveMessage, resetRoom } from './actions';
import { initialRoomDetailsState, IRoomDetailsState } from './state';

const reducer = createReducer<IRoomDetailsState>(
  initialRoomDetailsState,
  on(getRoom.success, (state, { room }) => ({
    ...state,
    room
  })),
  on(getMessages.success, (state, { messages }) => ({
    ...state,
    messages
  })),
  on(receiveMessage, (state, { message }) => ({
    ...state,
    messages: [...state.messages, message]
  })),
  on(resetRoom, state => ({
    ...state,
    room: {id: null, name: '', password: '', messages: []}
  }))
);

export function roomDetailsReducer(state: IRoomDetailsState, action: Action) {
  return reducer(state, action);
}
