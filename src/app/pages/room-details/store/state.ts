import { IMessage } from "src/app/shared/interfaces/message";
import { IRoom } from "src/app/shared/interfaces/room";

export const initialRoomDetailsState: IRoomDetailsState = {
  room: {id: null, name: '', password: ''},
  messages: []
};

export interface IRoomDetailsState {
  room: IRoom;
  messages: IMessage[];
}
