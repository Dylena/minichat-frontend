import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IRoomDetailsState } from './state';

const featureSelector = createFeatureSelector<IRoomDetailsState>('room-details');

const getRoom = createSelector(featureSelector, state => state.room);
const getMessages = createSelector(featureSelector, state => state.messages);

export const roomDetailsQuery = {
  getRoom,
  getMessages
};
