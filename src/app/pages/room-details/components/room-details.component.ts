import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Socket } from 'ngx-socket-io';
import { distinctUntilChanged, map, tap } from 'rxjs/operators';
import { IMessage } from 'src/app/shared/interfaces/message';
import { LoginFacade } from '../../login/store/facade';
import { RoomDetailsFacade } from '../store/facade';
import { TickTackToeModalComponent } from './tick-tack-toe/tick-tack-toe.component';

@Component({
  selector: 'app-room-details',
  templateUrl: './room-details.component.html',
  styleUrls: ['./room-details.component.scss'],
})
export class RoomDetailsComponent {
  messages$ = this.roomDetailsFacade.messages$;
  room$ = this.roomDetailsFacade.room$;
  user$ = this.loginFacade.user$;

  inputForm = this.fb.group({
    message: [],
  });

  chatMessages$ = this.socket.fromEvent<IMessage>('chat').pipe(
    map(message => message),
    distinctUntilChanged(),
    tap(message => this.roomDetailsFacade.receiveMessage(message)));

  constructor(
    private roomDetailsFacade: RoomDetailsFacade,
    private loginFacade: LoginFacade,
    private router: Router,
    private fb: FormBuilder,
    private socket: Socket,
    private ngbModal: NgbModal,
  ) {}

  back(): void {
    this.router.navigate(['/rooms']);
  }
  onSubmit(id: number, sender: number): void {
    console.log(sender);
    const value = { data: this.inputForm.value.message, sender, roomId: id };
    this.roomDetailsFacade.addMessage(value);
    this.inputForm.setValue({ message: [] });
  }
  tickTackToe(): void {
    const modal = this.ngbModal.open(TickTackToeModalComponent, {
      centered: true,
    });
  }
}
