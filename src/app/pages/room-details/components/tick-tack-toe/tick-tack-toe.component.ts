import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { isWithStatement } from 'typescript';
import _isEqual from 'lodash/isEqual'

enum State {
  None = '',
  X = 'X',
  O = 'O',
}

@Component({
  selector: 'app-tick-tack-toe',
  templateUrl: './tick-tack-toe.component.html',
  styleUrls: ['./tick-tack-toe.component.scss'],
})
export class TickTackToeModalComponent {
  private counter = 1;

  title = 'Tic-Tac-Toe';
  ticTacArray: State[][] = [
    [State.None, State.None, State.None],
    [State.None, State.None, State.None],
    [State.None, State.None, State.None],
  ];
  isWinOrDraw = '';

  constructor(public activeModal: NgbActiveModal) {}

  ticTac(i: number, j: number): void {
    if (this.ticTacArray[i][j] === State.None) {
      if (this.counter % 2 === 1) {
        this.ticTacArray[i][j] = State.X;
        this.winOrDraw(State.X);
      } else {
        this.ticTacArray[i][j] = State.O;
        this.winOrDraw(State.O);
      }
      this.counter++;
    }
  }

  winOrDraw(state: State): void {
    if (
      _isEqual(this.ticTacArray[0], [state, state, state]) ||
      _isEqual(this.ticTacArray[1], [state, state, state]) ||
      _isEqual(this.ticTacArray[2], [state, state, state])
    ) {
      this.isWinOrDraw = `${state} won`;
    }  else if (
      _isEqual(this.ticTacArray.map(row => row[0]), [state, state, state]) ||
      _isEqual(this.ticTacArray.map(row => row[1]), [state, state, state]) ||
      _isEqual(this.ticTacArray.map(row => row[2]), [state, state, state])
    ) {
      this.isWinOrDraw = `${state} won`;
    } else if (
      this.ticTacArray[0][0] === state &&
      this.ticTacArray[1][1] === state &&
      this.ticTacArray[2][2] === state
    ) {
      this.isWinOrDraw = `${state} won`;
    } else if (
      this.ticTacArray[2][0] === state &&
      this.ticTacArray[1][1] === state &&
      this.ticTacArray[0][2] === state
    ) {
      this.isWinOrDraw = `${state} won`;
    }
    if (this.counter >= 9 && !this.isWinOrDraw) {
      this.isWinOrDraw = 'Draw';
    }
  }

  resetGame() {
    this.ticTacArray = [
      [State.None, State.None, State.None],
      [State.None, State.None, State.None],
      [State.None, State.None, State.None],
    ];
    this.isWinOrDraw = '';
    this.counter = 1;
  }
}
