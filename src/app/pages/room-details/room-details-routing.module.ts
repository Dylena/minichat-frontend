import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckUserGuard } from 'src/app/shared/guards/check-user.guard';
import { RoomDetailsComponent } from './components/room-details.component';
import { RoomDetailsGuard } from './room-details.guard';


const routes: Routes = [
  {
    path: '',
    canActivate: [RoomDetailsGuard, CheckUserGuard],
    canDeactivate: [RoomDetailsGuard],
    component: RoomDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomDetailsRoutingModule { }
