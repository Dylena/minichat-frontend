import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoomDetailsRoutingModule } from './room-details-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { RoomDetailsComponent } from './components/room-details.component';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { RoomDetailsGuard } from './room-details.guard';
import { RoomDetailsFacade } from './store/facade';
import { RoomDetailsEffects } from './store/effects';
import { roomDetailsReducer } from './store/reducer';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { TickTackToeModalComponent } from './components/tick-tack-toe/tick-tack-toe.component';


const config: SocketIoConfig = { url: 'http://localhost:8080', options: {} };
@NgModule({
  declarations: [RoomDetailsComponent, TickTackToeModalComponent],
  imports: [
    CommonModule,
    RoomDetailsRoutingModule,
    SharedModule,
    StoreModule.forFeature('room-details', roomDetailsReducer),
    EffectsModule.forFeature([RoomDetailsEffects]),
    SocketIoModule.forRoot(config),
  ],
  providers: [RoomDetailsEffects, RoomDetailsFacade, RoomDetailsGuard]
})
export class RoomDetailsModule { }
