import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanDeactivate } from '@angular/router';
import { RoomDetailsFacade } from './store/facade';

@Injectable()
export class RoomDetailsGuard implements CanActivate, CanDeactivate<void> {
  constructor(private roomDetailsFacade: RoomDetailsFacade) {}
  canActivate(route: ActivatedRouteSnapshot): boolean {
    const { id } = route.params;
    if (id) {
      this.roomDetailsFacade.getRoom(id);
      this.roomDetailsFacade.getMessages(id);
    }
    return true;
  }

  canDeactivate(): boolean {
    this.roomDetailsFacade.resetRoom();
    return true;
  }
}
