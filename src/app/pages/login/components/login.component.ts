import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SignupModalComponent } from 'src/app/pages/login/components/signup-modal/signup-modal.component';
import { IUser } from 'src/app/shared/interfaces/user';
import { LoginFacade } from '../store/facade';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  loginForm = this.fb.group({
    name: [''],
    email: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    private loginFacade: LoginFacade,
    private ngbModal: NgbModal
  ) {}

  login(form: FormGroup): void {
    if (form.valid) {
      this.loginFacade.login(form.value);
    }
  }

  signup(): void {
    const modal = this.ngbModal.open(SignupModalComponent, {
      centered: true,
    });
    modal.componentInstance.title = 'Signup';
    modal.result
      .then((data: IUser) => {
        this.loginFacade.signup(data);
        this.loginForm.setValue({
          name: '',
          email: '',
          password: '',
        });
      }).catch((err) => {
        console.log(err);
      });
  }
}
