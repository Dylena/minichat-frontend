import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './components/login.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SignupModalComponent } from './components/signup-modal/signup-modal.component';

@NgModule({
  declarations: [
    LoginComponent,
    SignupModalComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedModule,
  ],
})
export class LoginModule { }
