import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { IUser } from 'src/app/shared/interfaces/user';
import { getUser, listUsers, login, logout, signup } from './actions';
import { loginQuery } from './selectors';

@Injectable()
export class LoginFacade {
  isLoggedIn$ = this.store.pipe(select(loginQuery.isLoggedIn));
  user$ = this.store.pipe(select(loginQuery.user));
  allUsers$ = this.store.pipe(select(loginQuery.allUsers));

  constructor(private store: Store) {}

  login(user: { email: string; password: string }): void {
    this.store.dispatch(login.fetch(user));
  }

  logout(): void {
    this.store.dispatch(logout.fetch());
  }

  signup(user: IUser): void {
    this.store.dispatch(signup.fetch({user}));
  }

  getUser(): void {
    this.store.dispatch(getUser.fetch());
  }
  listUsers(): void {
    this.store.dispatch(listUsers.fetch());
  }
}
