import { Action, createReducer, on } from '@ngrx/store';
import { getUser, listUsers, login, logout } from './actions';
import { ILoginState, initialLoginState } from './state';

const reducer = createReducer<ILoginState>(
  initialLoginState,
  on(login.success, (state) => ({
    ...state,
    isLoggedIn: true,
  })),
  on(logout.success, (state) => ({
    ...state,
    isLoggedIn: false,
  })),
  on(getUser.success, (state, { user }) => ({
    ...state,
    user: {name: user.name, email: user.email, id: user.id}
  })),
  on(listUsers.success, (state, { users }) => ({
    ...state,
    allUsers: users,
  })),
);

export function loginReducer(state: ILoginState, action: Action) {
  return reducer(state, action);
}
