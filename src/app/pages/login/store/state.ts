import { IUser } from "src/app/shared/interfaces/user";

export const initialLoginState: ILoginState = {
  isLoggedIn: false,
  user: {name: '', email: '', id: null},
  errorMessage: '',
  allUsers: [],
};

export interface ILoginState {
  isLoggedIn: boolean;
  user: IUser;
  errorMessage: string;
  allUsers: IUser[];
}
