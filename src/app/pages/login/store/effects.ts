import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { IUser } from 'src/app/shared/interfaces/user';

import { ApiService } from 'src/app/shared/services/api.service';
import { getUser, login, logout, signup, listUsers } from './actions';

@Injectable()
export class LoginEffects {

  login$ = createEffect(() =>
    this._actions$.pipe(
      ofType(login.fetch),
      switchMap((user: {email: string, password: string}) => {
        return this.apiService.login(user).pipe(
          map(() => {
            return login.success();
          }),
          catchError(err => {
            return of(login.fail({ error: err.message }));
          })
        );
      })
    )
  );

  loginSuccess$ = createEffect(() =>
    this._actions$.pipe(
      ofType(login.success),
      map(() => getUser.fetch()),
      tap(() => this.router.navigate(['rooms']))
    )
  );


  logout$ = createEffect(() =>
    this._actions$.pipe(
      ofType(logout.fetch),
      switchMap(() => {
        return this.apiService.logout().pipe(
          map(() => {
            return logout.success();
          }),
          catchError(err => {
            return of(logout.fail({ error: err.message }));
          })
        );
      })
    )
  );

  signup$ = createEffect(() =>
    this._actions$.pipe(
      ofType(signup.fetch),
      switchMap(({user}) => {
        return this.apiService.signup(user).pipe(
          map(user => {
            return signup.success({user});
          }),
          catchError(err => {
            return of(signup.fail({error: err.message}));
          })
        )
      })
    )
  );

  getUser$ = createEffect(() =>
    this._actions$.pipe(
      ofType(getUser.fetch),
      switchMap(() => {
        return this.apiService.getUser().pipe(
          map((user: IUser) => {
            return getUser.success({user});
          }),
          catchError(err => {
            return of(getUser.fail({ error: err.message }));
          })
        );
      })
    )
  );

  listUsers$ = createEffect(() =>
    this._actions$.pipe(
      ofType(listUsers.fetch),
      switchMap(() => {
        return this.apiService.listUsers().pipe(
          map((users: IUser[]) => {
            return listUsers.success({users});
          }),
          catchError(err => {
            return of(listUsers.fail({ error: err.message }));
          })
        );
      })
    )
  );

  constructor(private _actions$: Actions, private apiService: ApiService, private router: Router) {}
}
