import { props } from '@ngrx/store';
import { IUser } from 'src/app/shared/interfaces/user';
import { createApiAction } from 'src/app/shared/utils/ngrx.utils';

export const login = createApiAction('[Login Actions] login', {
  fetch: props<{ email: string; password: string }>(),
  fail: props<{ error: string }>(),
});

export const logout = createApiAction('[Login Actions] logout', {
  fail: props<{ error: string }>(),
});

export const signup = createApiAction('[Login Actions] signup', {
  fetch: props<{ user: IUser }>(),
  success: props<{ user: IUser }>(),
  fail: props<{ error: string }>(),
});

export const getUser = createApiAction('[Login Actions] getUser', {
  success: props<{ user: IUser }>(),
  fail: props<{ error: string }>(),
});

export const listUsers = createApiAction('[Login Actions] listUsers', {
  success: props<{ users: IUser[] }>(),
  fail: props<{ error: string }>(),
});
