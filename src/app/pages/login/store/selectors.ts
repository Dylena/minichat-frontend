import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ILoginState } from './state';

const featureSelector = createFeatureSelector<ILoginState>('login');

const isLoggedIn = createSelector(featureSelector, state => state.isLoggedIn);
const user = createSelector(featureSelector, state => state.user)
const allUsers = createSelector(featureSelector, state => state.allUsers);

export const loginQuery = {
  isLoggedIn,
  user,
  allUsers
};
